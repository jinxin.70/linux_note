# Linux系统学习(尚硅谷Linux经典升级)

虚拟机三种网络连接方式：

![](Snipaste_2018-04-05_22-47-39.png)

> Linux至少分3个分区：

> /root

>/swap

> /boot

> /是根目录，树状目录结构

> /dev  cpu、disk

> /media  dvd、usb

***一切皆文件***

## Linux的目录结构

* /lib

> 系统需要的最基本的动态链接共享库

* /lost+found

> 非法关机存放的一些文件

* /etc

> 配置文件目录

* /usr

> 用户的很多程序和文件都放在这个目录下，类似windows下的program files目录

* /boot

> 存放启动Linux时使用的一些核心文件，包括一些连接文件以及镜像文件

* /proc

> 内核相关目录，是一个虚拟目录，系统内存的映射，访问这个目录获取系统信息

* /srv

> 内核相关目录，service的缩写，存放服务启动之后需要提取的数据

* /sys

注意： **内核相关目录，别动**

* /tmp

* /dev

> 硬件设备

* /media

> 自动识别的一些设备，如外置存储

* /mnt

> 临时挂载的别的文件系统

* /opt

> 额外按照的软件存放的目录

* /usr/local

> 额外安装软件后所安装的目录，一般是通过编译源码方式安装的程序

* /var

> 存放不断扩充的东西，经常被修改的目录一般放在这里，比如日志

* /selinux

> 安全子系统，能控制程序只能访问特定文件。黑客入侵触发。

## 实操篇

>远程连接，需要开启sshd服务

> centos命令行运行``setup``查看系统服务下是否启动了，*表示使用了。该服务会监听22端口

### VIM编辑器

vi的三种常见模式：

1.  正常模式，阅读文件，可以使用快捷键

* ``yyp`` 向下复制当前行

* ``5yyp`` 拷贝当前行向下5行，粘贴。p是粘贴

* ``dd`` 删除当前行

* ``5dd`` 删除光标下的5行

* 查找文本：输入``/``或者``?``,后面紧跟要搜索的文本，回车。按n想下找，N向上找

* ``u``   撤销上一步的操作

* ``Ctrl+r`` 恢复上一步被撤销的操作

* 设置(``:set nu``)与取消行号(``:set nonu``)

* 到顶部，按两次[[

* 到底部，按两次]]

* 到文档某个位置，``50%``

* 到指定位置，比如到第10行，输入10，然后shift+g

2.  插入/编辑模式，可以输入内容

* ``i`` 当前光标位置插入开始编辑

* ``o`` 下一行编辑

3.  命令行模式，可以完成读取、存盘、替换、离开vim、显示行号

* ``:wq`` 保存退出

* ``:q`` 退出

* ``:q!``强制退出

## 关机和重启

> shutdown -h now 立即关机

> shutdown -h 1 一分钟后关机

> shutdown -r now 现在重启

> halt 关机

> reboot 重启

> sync 把内存数据同步到磁盘

## 用户管理

### 添加用户

> ``useradd foo``

> 如果没有指定组，会为用户foo创建一个foo用户组,并创建一个该用户的家目录

> ``useradd -d 用户名 /home/ba``

> -d指定家目录

> ``passwd 用户名``  设置密码

### 删除用户

> ``userdel 用户名``

不带参数，会保留家目录

``userdel -r  用户名``

会同时删除家目录

> 一般保存家目录，因为可能有一些个人资料

### 查询用户

> ``id 用户名``

### 切换用户

> su - 普通用户

> su root

> exit 或者ctrl+d 回到原来的用户

### 用户组管理

> groupadd 组名

> groupdel 组名

> 增加用户时直接加组：useradd -g 用户组 用户名

> 修改用户组：usermod -g 用户组 用户名

> 用户和组的相关文件

/etc/passwd

用户的配置文件

/etc/shadow

口令的配置文件：每行的含义：

/etc/group

组的配置文件

### 指定运行级别

![](指定运行级别.png)

> /etc/inittab

> init [0123456]

### 找回root密码

进入单用户模式，单用户(init 1)不需要密码就可以登录，然后passwd root改密码

## 文件目录类

``pwd``

``ls -alFh`` 

> *h*表示人类易读的形式

``cd ``

``cd ~``

``cd ../``

``mkdir -p /home/ba/far``

``rm -rf`` 目录及目录下的内容

``touch filename``  创建一个文件

``cp -r source dest`` 递归拷贝

``rm`` -r:递归删除整个文件夹  -f:强制删除

``mv`` 移动文件与目录或重命名

``cat`` 浏览文件内容 -n:行号 | more 分页显示，并显示行号

``more`` 文件 

``less`` 根据显示需要加载的内容。对于大文件效率高

``>`` 重定向文件,没有文件会创建文件，文件已存在会覆盖内容

``>>`` 追加内容到文件

``echo`` 输出内容到控制台

``head 文件``  查看文件头10行内容

``head -n 5 文件``  查看文件头5行内容

``tail -f 文件``  实时追踪文件的更新内容

``tail -n 5 文件``

``date``

``date +%Y``

``date +%m``

``date +%d``

``date "+%Y-%m-%d %H:%M:%S"``

``date -s "2018-10-10 11:22:22"``  设置系统时间

``cal`` 显示日历

``cal 2018`` 显示2018年的整年日历

### 搜索查找类

> find

> -name 根据名称

> -user 根据属于哪个用户的

> -size 根据大小

``find /home -name 文件名``

``find /opt -user root`` 查找/opt下属于root的文件

``find / -size +20M`` 查找大于20M的文件

``find / -size -20M`` 小于20M

``find / -size 20M`` 等于20M

``locate`` 基于索引的查找，速度较快，需要定期更新locate，使用``updatedb``

``grep`` 和管道符``|``  查找文件内容

``grep [-i|-n] 查找内容 源文件``

> -n 显示匹配行及行号

> -i 忽略大小写

> 管道符就是将前一个命令的输出作为后一个命令的输入

``gzip 文件`` 压缩,不会保留源文件

``gunzip 文件.gz`` 解压

``zip -r 文件.zip ./`` 将当前文件夹下的内容压缩为文件.zip

``unzip -d 目标文件夹 文件名``

``tar -xzvf 文件`` 解压tar文件

> -c 产生.tar打包文件

> -v 显示详细信息

> -f 指定压缩后的文件名

> -z 打包同时压缩

> -x 解包.tar文件

``tar -zcvf a.tar.gz 文件名1 文件名2`` 将两个文件压缩为指定压缩文件a.tar.gz

``tar -zcvf myhome.tar.gz /home/`` 压缩home目录为myhome.tar.gz

``tar -xzvf myhome.tar.gz -C /home`` 解压到指定目录,目录必须存在

## 组管理

```shell
groupadd police
useradd -g police tom
passwd tom
su tom
touch a.txt
```

会查看文件属于哪个用户，哪个组，及权限

用户 用户组

递归更改文件夹的所有者

``chown -R root:root 文件或者文件夹``

![文件(夹)权限的介绍](./文件(夹)权限的介绍.png)

可用数字表示：r=4,w=2,x=1

rwx=4+2+1=7

中间的数字：如果是目录，表示子文件夹数，如果是文件，表示硬链接数

目录是一个特殊文件，ls -alFh查看的文件夹大小，是这个文件夹本身的大小，不是总大小

最后的时间是最后文件修改时间

![会查看文件和文件夹信息](./会查看文件和文件夹信息.png)

### 修改权限

``chmod [+|-|=][r|w|x] [file|diretory]``

```shell
chmod u=rwx,g=rx,o=x 文件或目录名

chmod o+w

chmod a-x
```

rwx=4+2+1=7

r-x=4+1=5

r-x=4+1=5

自己可以读写执行，同组用户和其他用户，不可以写

```shell
chmod 755 /home/filename
```

改变文件所在组

``chgrp newgroup file``

## 定时任务调度

``crontab``

-e 编辑定时任务

-l 查看

-r 终止(删除)定时任务

service crond restart 重启任务调度

![crond任务调度](./crond任务调度.png)

![cron参数细节](./cron参数细节.png)

![定时任务案例](./定时任务案例.png)

## 磁盘分区和挂载

![分区的基础知识](./分区的基础知识.png)

![win下的磁盘分区](./win下的磁盘分区.png)

![linux分区原理](./linux分区原理.png)

### 硬盘

IDE 并口

SCSI 串口，性能更好，目前基本都是这种硬盘

![硬盘说明](./硬盘说明.png)

查看硬盘

``lsblk -f``

下面不带参数的命令可以看到分区大小

``lsblk``

助记：老师不离开

待续：给linux添加一块新硬盘

1. 虚拟机添加硬盘

手动添加硬盘

2. 分区

``fdisk /dev/sdb``

![fdisk命令分区步骤](fdisk命令分区步骤.png)

3. 格式化

将设备分区格式化为ext4格式的文件系统

``mkfs -t ext4 /dev/sdb1``

4. 挂载

创建目录，并挂载到创建的目录

```
mkdir /home/newdisk
mount /dev/sdb1 /home/newdisk/
```

> 另外，卸载命令

> umount 设备名称 或者 挂载目录 

5. 设置可以自动挂载

在``/etc/fstab``文件中添加一行

```
vi /etc/fstab
/dev/sdb1                                 /home/newdisk           ext4    defaults        0 0
```

退出文件编辑，允许下面命令

``mount -a``

## 磁盘情况查询

``df -h``

### 查看指定目录的磁盘占用情况

du -sh /目录

默认为当前目录

-s

指定目录占用大小汇总

-h 

带计量单位

-a 

含文件

--max-depth=1

子目录深度

-c

列出明细的同时，增加汇总值

查询/opt目录的磁盘占用情况，深度为1

``du -ach --max-depth=1 /opt``

### 磁盘情况-常用

1. 统计/home文件夹下的文件个数

wc表示统计

``l /home | grep "^-" | wc -l``

2. 统计/home文件夹目录个数

包括``./``和``../``

``l /home | grep "^-" | wc -l``

3. 统计/home文件夹文件的个数，包括子目录

-R表示递归子目录

``l -R /home | grep "^-" | wc -l``

4. 统计文件夹下目录的个数，包括子文件夹里的

``l -R /home | grep "^d" | wc -l``

5. 以树状显示文件目录

```
yum install tree
tree
```

## 网络配置

``vi /etc/sysconfig/network-scripts/ifcfg-eth0``

## 进程管理

### 进程查询

> ``ps -ef | grep 'java'``

-e:显示所有进程

-f:全格式

ps显示的信息选项

|字段|说明|
|:---|:---|
|PID|进程识别号|
|TTY|终端机号|
|TIME|此进程所消CPU时间|
|CMD|正在执行的命令或进程名|

``ps -aux``

ps -a:显示当前终端的所有进程信息

ps -u:以用户的格式显示进程信息

ps -x:显示后台进程运行的参数

![ps详解](ps详解.png)

### 进程终止

kill -9

-9:表示强迫进程立即终止

### #如何踢掉非法用户

1. 找到ssh登录的进程ID

``ps -aux | grep sshd``

2. kill掉对应进程id

## pstree查看进程树

-p:显示进程的ID

-u:显示进程的所属用户

## 查看防火墙状态

``service iptables status``

win上可以通过telnet查看端口是否在监听

使用``chkconfig``让维持服务状态

### 查看服务名

1. setup => 系统服务

2. /etc/init.d服务名称

使用``ls -l /etc/init.d/``

列出系统有哪些服务

## 开机流程和服务

![开机流程](开机流程.png)

### 服务管理``chkconfig``

通过chkconfig命令可以给各个运行级别设置自启动或者关闭

语法：

1. 查看服务：``chkconfig --list|grep xxx``

2. ``chkconfig 服务名 --list``

3. ``chkconfig --level 5 服务名 on/off``

## 监控服务

### #top

选项说明：

-d 秒数

指定每隔几秒更新，默认3秒

-i

使top不显示任何闲置或者僵死进程

-p

指定进程id监控某个进程状态

交互操作说明：

P 以CPU使用率排序，默认此项

M 以内存使用率排序

N 以PID排序

q 退出top

### #ps

### #网络监控

netstat -anp | more

-an 按一定顺序排列输出

-p 显示哪个进程调用





